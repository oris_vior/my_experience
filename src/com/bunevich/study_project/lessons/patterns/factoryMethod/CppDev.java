package com.bunevich.study_project.lessons.patterns.factoryMethod;

public class CppDev implements Developer{
    @Override
    public void writeCode() {
        System.out.println("C++");
    }
}
