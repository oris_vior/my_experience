package com.bunevich.study_project.lessons.patterns.factoryMethod;

public interface Developer {
    void writeCode();
}
