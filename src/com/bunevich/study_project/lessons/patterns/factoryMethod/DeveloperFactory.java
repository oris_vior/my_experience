package com.bunevich.study_project.lessons.patterns.factoryMethod;

public interface DeveloperFactory {
    Developer createDeveloper();
}
