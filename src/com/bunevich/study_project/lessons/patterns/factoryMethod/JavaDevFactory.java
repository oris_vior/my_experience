package com.bunevich.study_project.lessons.patterns.factoryMethod;

public class JavaDevFactory implements DeveloperFactory{
    @Override
    public Developer createDeveloper() {
        return new JavaDev();
    }
}
