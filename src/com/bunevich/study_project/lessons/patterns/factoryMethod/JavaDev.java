package com.bunevich.study_project.lessons.patterns.factoryMethod;

public class JavaDev implements Developer{
    @Override
    public void writeCode() {
        System.out.println("Java");
    }
}
