package com.bunevich.study_project.lessons.patterns.factoryMethod;

public class Main {
    public static void main(String[] args) {
        DeveloperFactory developerFactory = createDeveloperBySpeciality("java");
    }

    static DeveloperFactory createDeveloperBySpeciality(String speciality){
        if(speciality.equalsIgnoreCase("java"))
        {
            return new JavaDevFactory();
        }
        else if(speciality.equalsIgnoreCase("c++")){
            return new CppDevFactory();
        }
        else {
            throw new RuntimeException(speciality + " is unknown speciality");
        }
    }

}
