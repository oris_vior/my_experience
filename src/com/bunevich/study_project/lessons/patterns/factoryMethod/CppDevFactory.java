package com.bunevich.study_project.lessons.patterns.factoryMethod;

public class CppDevFactory implements DeveloperFactory{
    @Override
    public Developer createDeveloper() {
        return new CppDev();
    }
}
