package com.bunevich.study_project.lessons.hackerrank.problem1;

import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.stream.Collectors;

public class Main {
    public static void main(String[] args) {
       Class<Object> student = Object.class;
        ArrayList<String> methods = new ArrayList<>();

        for (Method method: student.getDeclaredMethods()){
            methods.add(method.getName());
        }
        methods = (ArrayList<String>) methods.stream().sorted().collect(Collectors.toList());
        for (String str :methods) {
            System.out.println(str);
        }

    }
}
