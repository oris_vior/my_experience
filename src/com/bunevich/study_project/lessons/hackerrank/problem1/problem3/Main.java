package com.bunevich.study_project.lessons.hackerrank.problem1.problem3;




interface BaseI { void method(); }

class BaseC
{
    public void method()
    {
        System.out.println("Inside BaseC::method");
    }
}

class ImplC extends BaseC implements BaseI
{
    public static void main(String []s)
    {
        Integer [] in = new Integer[1];
        (new ImplC()).method();
    }
}