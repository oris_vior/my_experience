package com.bunevich.study_project.lessons.lesson3;


import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Locale;


public class ShoppingList {
    public static final BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));

    public static void main(String[] args) throws IOException {
        ArrayList<String> listOfFood = new ArrayList<>();
        System.out.println("=======Welcome======");
        startMenuList(listOfFood);
    }

    public static void startMenuList(ArrayList<String> listOfFood) throws IOException {
        System.out.println("\n" + "Make your choice: ");
        System.out.println("View product list - 1");
        System.out.println("Add new product - 2");
        System.out.println("Delete some product - 3");
        System.out.println("Exit - 4");
        System.out.print("Enter: ");
        String userChooseInStartMenu = reader.readLine();
        whenUserMadeChoose(userChooseInStartMenu, listOfFood);
    }

    public static void whenUserMadeChoose(String userChooseInStartMenu, ArrayList<String> listOfFood) throws IOException {
        switch (userChooseInStartMenu) {
            case "1" -> ifUserInputOne(listOfFood);
            case "2" -> ifUserSelectTwo(listOfFood);
            case "3" -> ifUserSelectThree(listOfFood);
            case "4" -> ifUserSelectFour(listOfFood);
            default -> {
                System.out.println("Not correct");
                startMenuList(listOfFood);
            }
        }
    }

    public static void ifUserInputOne(ArrayList<String> listOfFood) throws IOException {
        if (!listOfFood.isEmpty()) {
            for (int i = 0; i < listOfFood.size(); i++) {
                System.out.println((i + 1) + ". " + listOfFood.get(i));
            }
        } else {
            System.out.println("Your list is empty");
            ifUserSelectTwo(listOfFood);
        }
        startMenuList(listOfFood);
    }

    public static void ifUserSelectTwo(ArrayList<String> listOfFood) throws IOException {
        System.out.println("\n" + "Add new product");
        System.out.println("If you want to exit to the Menu enter \"1\" ");
        String newProductInList = brandNameInFood();

        while (!newProductInList.equals("1")) {
            listOfFood.add(newProductInList);
            System.out.println("OK!");
            newProductInList = brandNameInFood();
        }
        startMenuList(listOfFood);
    }

    public static void ifUserSelectThree(ArrayList<String> listOfFood) throws IOException {
        for (int i = 0; i < listOfFood.size(); i++) {
            System.out.println((i + 1) + ". " + listOfFood.get(i));
        }
        System.out.print("\n" + "Enter index element of food want you delete: ");
        try {
            String numberForDeleteFoodFromTheList = reader.readLine();
            int convertStringToInt = Integer.parseInt(numberForDeleteFoodFromTheList) - 1;
            listOfFood.remove(convertStringToInt);
            ifUserInputOne(listOfFood);
        } catch (NumberFormatException ex) {
            ex.printStackTrace();
            System.out.println("Only numbers");
            startMenuList(listOfFood);
        }
    }

    public static void ifUserSelectFour(ArrayList<String> listOfFood) throws IOException {
        System.out.println("\n" + "Enter: \"That`s all\" for Exit");
        String fieldForExit = reader.readLine().trim().toLowerCase(Locale.ROOT);
        if (fieldForExit.trim().equals("that`s all") || fieldForExit.equals("thats all")) {
            System.out.println("Goodbye!");
            return;
        }
        startMenuList(listOfFood);
    }

    public static String brandNameInFood() throws IOException {
        String stringForChangesWithBrand = reader.readLine().trim().toLowerCase(Locale.ROOT);

        if (stringForChangesWithBrand.contains("roshen")) {
            stringForChangesWithBrand = stringForChangesWithBrand.replace("roshen", "Sweetness");
        } else if (stringForChangesWithBrand.contains("beer")) {
            System.out.println("Not today,bro");
            stringForChangesWithBrand = "Carrot";
        }
        return productsWithEndingSOREs(stringForChangesWithBrand);

    }

    public static String productsWithEndingSOREs(String stringForReturnManyFoods) throws IOException {
        if (stringForReturnManyFoods.endsWith("es") || stringForReturnManyFoods.endsWith("s")) {
            stringForReturnManyFoods = forAmountOfFood(stringForReturnManyFoods);
        }
        return stringForReturnManyFoods;
    }

    public static String forAmountOfFood(String stringForReturnManyFoods) throws IOException {
        System.out.print("How mach? : ");
        String amountFoods = reader.readLine();
        return stringForReturnManyFoods + " amount: " + amountFoods;
    }
}