package com.bunevich.study_project.lessons.parser.XML;

import com.bunevich.study_project.lessons.parser.XML.model.Catalog;
import com.bunevich.study_project.lessons.parser.XML.model.Cd;
import org.w3c.dom.Document;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.stream.Collectors;

public class ParserXML {
    public static void main(String[] args) {
        Catalog catalog = new Catalog();
        Document doc = null;
        try {
            doc = buildDocument();
        } catch (Exception e) {
            System.out.println("Error: " + e.getMessage());
        }

        assert doc != null;
        Node catalog1 = doc.getFirstChild();
        NodeList cdList = catalog1.getChildNodes();
        for (int i = 0; i < cdList.getLength(); i++) {
            Node cd = cdList.item(i);
            if (cd.getNodeType() != Node.ELEMENT_NODE) {
                continue;
            }
            NodeList info = cd.getChildNodes();
            Cd tempCd = new Cd();
            for (int j = 0; j < info.getLength(); j++) {
                Node infoText = info.item(j);


                switch (infoText.getNodeName().toLowerCase(Locale.ROOT)) {
                    case "title" -> {
                        tempCd.setTitle(infoText.getTextContent());
                        break;
                    }
                    case "artist" -> {
                        tempCd.setArtist(infoText.getTextContent());
                        break;
                    }
                    case "country" -> {
                        tempCd.setCountry(infoText.getTextContent());
                        break;
                    }
                    case "company" -> {
                        tempCd.setCompany(infoText.getTextContent());
                        break;
                    }
                    case "price" -> {
                        tempCd.setPrise(Double.parseDouble(infoText.getTextContent()));
                        break;
                    }
                    case "year" -> {
                        tempCd.setYear(Integer.parseInt(infoText.getTextContent()));
                        break;
                    }
                }

            }
            catalog.setCd(tempCd);
        }
        System.out.println( catalog.toString());

        List<String> name = (List<String>) catalog.getArrayList().stream().map(Cd::getArtist).toList();
        System.out.println(name);

    }

    private static Document buildDocument() throws ParserConfigurationException, IOException, SAXException {
        File file = new File("src/com/bunevich/study_project/lessons/parser/XML/catalog.XML");
        DocumentBuilderFactory documentBuilder = DocumentBuilderFactory.newInstance();
        return documentBuilder.newDocumentBuilder().parse(file);
    }
}
