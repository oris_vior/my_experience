package com.bunevich.study_project.lessons.parser.XML.model;

import java.util.ArrayList;
import java.util.List;

public class Catalog {
    private final List<Cd> arrayList = new ArrayList<>();


    public List<Cd> getArrayList() {
        return arrayList;
    }

    public void setCd(Cd cd) {
        arrayList.add(cd);
    }


    @Override
    public String toString() {
        return "Catalog{" +
                "arrayList=" + arrayList +
                '}';
    }
}
