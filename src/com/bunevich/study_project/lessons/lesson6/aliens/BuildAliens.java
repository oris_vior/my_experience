package com.bunevich.study_project.lessons.lesson6.aliens;

public class BuildAliens {
    private Aliens aliens;
    public Aliens getAliens()
    {
        return aliens;
    }

    public BuildAliens addEyes(int eyes){
        aliens.setEyes(eyes);
        return this;
    }
    public BuildAliens addHands(int hands){
        aliens.setEyes(hands);
        return this;
    }
    public BuildAliens addLeg(int leg){
        aliens.setEyes(leg);
        return this;
    }
    public BuildAliens addHearts(int hearts){
        aliens.setEyes(hearts);
        return this;
    }
    public BuildAliens addFingers(int fingers){
        aliens.setFingers(fingers);
        return this;
    }
}
