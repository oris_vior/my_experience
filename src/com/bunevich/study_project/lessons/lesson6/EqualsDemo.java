package com.bunevich.study_project.lessons.lesson6;

import java.util.Objects;

public class EqualsDemo implements  Cloneable{

    private String id = "17";

    @Override
    protected Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        EqualsDemo that = (EqualsDemo) o;
        return Objects.equals(id, that.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

    @Override
    public String toString() {
        return "EqualsDemo{" +
                "id='" + id + '\'' +"HASH" +hashCode()+
                '}';
    }
}
