package com.bunevich.study_project.lessons.OOP;

public class Over extends TestOverETC{

    public Over(int x, int y) {
        super(x, y);
    }

    @Override
    int summ(int z) {
        return getX()-getY()-z;
    }
}
