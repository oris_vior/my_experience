package com.bunevich.study_project.lessons.OOP;

public class Test {
    public static void main(String[] args) {
        Encapsulation encapsulation = new Encapsulation();
        encapsulation.setAge(12);
        encapsulation.setName("qwe");
//        System.out.println(encapsulation.getAge());
//        System.out.println(encapsulation.getName());
        B b = new B();
        b.One();
        Constructor constructor = new Constructor(3,2,3);
        System.out.println(constructor.height);
        Dog dog = new Dog();
        AnimalOverriding dog1 = new Dog();
        dog.voise();

        dog1.voise();

        Cat cat = new Cat();
        cat.voise();


    }
    public static class Constructor{
        private  int weight;
        private  int height;
        private  int width;

        public Constructor(int weight, int height, int width) {
            this.weight = weight;
            this.height = height;
            this.width = width;
        }

    }

    public static class A{
        public int one = 18;
    }
    public static class B extends A{
        public  int one = 12;
        public void One(){
            System.out.println(super.one);
            System.out.println(one);
        }
    }

    public static class AnimalOverriding{
        public  void voise(){
            System.out.println("Sound");
        }
    }
    public static class Dog extends AnimalOverriding{
        @Override
        public void voise() {
            System.out.println("Gaw");
        }

    }
    public static class Cat extends AnimalOverriding{
        @Override
        public void voise() {
            System.out.println("Meow");
        }
    }
}
