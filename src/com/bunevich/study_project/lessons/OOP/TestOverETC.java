package com.bunevich.study_project.lessons.OOP;

import java.util.Objects;

public class TestOverETC  {

    private int x;
    private int y;
    private String name;

    public TestOverETC(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    int summ(int z)
        {
            return x+y+z;
        }
}
