package com.bunevich.study_project.lessons.sort;

public class Bubble {
    public static void main(String[] args) {
        int[] array= new int[]{3,1,2,5,6,50,15,14};

        for(int i = 0; i< array.length-1; i++){
            if(array[i]> array[i+1]){
                int temp = array[i];
                array[i]=array[i+1];
                array[i+1]=temp;
            }
        }
        for (Integer q: array) {
            System.out.println(q);
        }
    }
}
