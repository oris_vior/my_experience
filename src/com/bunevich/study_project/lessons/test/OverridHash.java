package com.bunevich.study_project.lessons.test;

public class OverridHash {
    int a;

    public OverridHash(int a) {
        this.a = a;
    }

    @Override
    public int hashCode() {
        return 0;
    }

    @Override
    public boolean equals(Object obj) {
        return false;
    }
}
