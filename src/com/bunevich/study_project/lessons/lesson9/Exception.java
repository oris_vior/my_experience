package com.bunevich.study_project.lessons.lesson9;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Exception {
    public static void main(String[] args) throws IOException {

        Exception demo = new Exception();
        demo.inputText();
    }

    public  String inputText() throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        String input = "";
        while (!input.equals("stop")) {

                input = reader.readLine();
                try {
                    viewMessage(input);
                }
                catch (UnsupportedOperationException e ){
                    System.out.println(e.getMessage());
                }
            System.out.println(input);


        }
        return input;
    }

    public  void viewMessage(String input)  {
        if(input.equals("Igor")){
            throw new UnsupportedOperationException("Not Igor");
        }
//        else if (input.contains("3")){
//            throw new java.lang.Exception("Don`t like '3'");
//        }
        System.out.println(input);
    }
}
