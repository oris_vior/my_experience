package com.bunevich.study_project.lessons.final_project.list;



import com.bunevich.study_project.lessons.final_project.Product.ProductRange;
import com.bunevich.study_project.lessons.final_project.Product.TestTuple;

import java.util.Arrays;
import java.util.Locale;

public class ShoppingList {
    private static final int DEFAULT_LIST_CAPACITY = 20;

    private String[] shoppingItems;
    private int actualSize;
    private ProductRange demo = new ProductRange();


    public ShoppingList() {
        shoppingItems = new String[DEFAULT_LIST_CAPACITY];
    }

    public String addProductToList(String input) {
        checkListSize();
        for (TestTuple brand : demo.getArray()) {
            if (input.trim().toLowerCase(Locale.ROOT).equals(brand.getProduct().getBrand().toLowerCase(Locale.ROOT))) {
                // не забыть вставить отнимание продукта из количества  при добавлении
                shoppingItems[actualSize] = input;
                actualSize++;
                return ("Product \"" + input + "\" was added");
            }
        }
        return ("Not found");
    }

    private void checkListSize() {
        if (actualSize >= shoppingItems.length - 1) {
            shoppingItems = Arrays.copyOf(shoppingItems, shoppingItems.length * 2);
        }
    }

    public void viewShoppingList() {
        System.out.println("----Your products----");
        for (int i = 0; i < actualSize; i++) {
            System.out.println(shoppingItems[i]);
        }
    }

    public void viewProducts() {
        for (TestTuple name : demo.getArray()) {
            System.out.println(name.getProduct().getBrand() + " " + name.getResidue());
        }
    }

    public String removeElementForIndex(String input) {
        int index = 0;
        try {
            index = Integer.parseInt(input);
        }
        catch (Exception e){
            System.out.println(e);
        }
        if (index >= 0 && index <= actualSize - 1) {
            System.arraycopy(shoppingItems, index + 1, shoppingItems, index, shoppingItems.length - index - 1);
            actualSize--;
            return "Element was deleted";
        }
        return "Element not found";
    }

    public void removeElement(String input) {
        for (int i = 0; i < actualSize; i++) {
            if (input.trim().toLowerCase(Locale.ROOT).equals(shoppingItems[i].toLowerCase(Locale.ROOT))) {
                removeElementForIndex(String.valueOf(i));
            }
        }

    }

}




