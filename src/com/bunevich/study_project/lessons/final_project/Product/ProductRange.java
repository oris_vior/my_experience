package com.bunevich.study_project.lessons.final_project.Product;


import com.bunevich.study_project.lessons.final_project.Product.item.Bread;
import com.bunevich.study_project.lessons.final_project.Product.item.Bun;
import com.bunevich.study_project.lessons.final_project.Product.item.Cake;
import com.bunevich.study_project.lessons.final_project.Product.item.Candy;
import com.bunevich.study_project.lessons.final_project.Product.item.Milk;

public class ProductRange {

    private final Product bread1 = new Bread("dairy", "Bazhanovski", 14, "7 days", true);
    private final Product bread2 = new Bread("dairy", "For Toast", 12, "7 days", false);
    private final Product bread3 = new Bread("dairy", "Black bread", 17, "7 days", true);
    private final Product bread4 = new Bread("dairy", "With sunflower seeds", 7, "7 days", true);
    private final Product bun1 = new Bun("dairy", "with poppy", 7, "5 days", true);
    private final Product bun2 = new Bun("dairy", "with condensed milk", 20, "5 days", true);
    private final Product cake1 = new Cake("sweets", "Kyiv", 50, 1000);
    private final Product cake2 = new Cake("sweets", "Cheesecake", 84, 2500);
    private final Product candy1 = new Candy("sweets", "Milka", 10, 500);
    private final Product candy2 = new Candy("sweets", "Mars", 12, 500);
    private final Product candy3 = new Candy("sweets", "Snikers", 8, 500);
    private final Product cheese1 = new Milk("milk", "milk cheese", 200, 8);
    private final Product milk1 = new Milk("milk", "milk cheese", 20, 10);
    private final Product milk2 = new Milk("milk", "Prostokvasha", 27, 10);
    private final Product yogurt1 = new Milk("milk", "baby yogurt", 15, 8);
    private final Product yogurt2 = new Milk("milk", "adult yogurt", 12, 8);

    private final TestTuple Product1 = new TestTuple(bread1, 5);
    private final TestTuple Product2 = new TestTuple(bread2, 12);
    private final TestTuple Product3 = new TestTuple(bread3, 7);
    private final TestTuple Product4 = new TestTuple(bread4, 10);
    private final TestTuple Product5 = new TestTuple(bun1, 53);
    private final TestTuple Product6 = new TestTuple(bun2, 56);
    private final TestTuple Product7 = new TestTuple(cake1, 15);
    private final TestTuple Product8 = new TestTuple(cake2, 45);
    private final TestTuple Product9 = new TestTuple(candy1, 6);
    private final TestTuple Product10 = new TestTuple(candy2, 14);
    private final TestTuple Product11 = new TestTuple(candy3, 13);
    private final TestTuple Product12 = new TestTuple(cheese1, 53);
    private final TestTuple Product13 = new TestTuple(milk1, 4);
    private final TestTuple Product14 = new TestTuple(milk2, 3);
    private final TestTuple Product15 = new TestTuple(yogurt1, 2);
    private final TestTuple Product16 = new TestTuple(yogurt2, 6);

    private TestTuple[] array = new TestTuple[]
            {
                    Product1, Product2,
                    Product3, Product4,
                    Product5, Product6,
                    Product7, Product8,
                    Product9, Product10,
                    Product11, Product12,
                    Product13, Product14,
                    Product15, Product16
            };


    public TestTuple[] getArray() {
        return this.array;
    }
}
