package com.bunevich.study_project.lessons.final_project.Product;


public class TestTuple {
    private final Product product ;
    private final Integer residue;

    public TestTuple(Product product, Integer residue) {
        this.product = product;
        this.residue = residue;
    }



    public Product getProduct() {
        return product;
    }

    public Integer getResidue() {
        return residue;
    }
}
