package com.bunevich.study_project.lessons.final_project.Product;

public class Dairy extends Product {
    private final int storageTemperature;

    public Dairy(String type, String brand, int cost, int storageTemperature) {
        super(type, brand, cost);
        this.storageTemperature = storageTemperature;
    }
}
