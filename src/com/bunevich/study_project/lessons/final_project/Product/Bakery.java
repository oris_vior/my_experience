package com.bunevich.study_project.lessons.final_project.Product;

public class Bakery extends Product {
    private String shelfLife;
    private boolean friability;

    public Bakery(String type, String brand, int cost, String shelfLife, boolean friability) {
        super(type, brand, cost );
        this.shelfLife = shelfLife;
        this.friability = friability;
    }

    public String getShelfLife() {return shelfLife;}

    public void setShelfLife(String shelfLife) {
        this.shelfLife = shelfLife;
    }

    public boolean isFriability() {
        return friability;
    }

    public void setFriability(boolean friability) {
        this.friability = friability;
    }
}
