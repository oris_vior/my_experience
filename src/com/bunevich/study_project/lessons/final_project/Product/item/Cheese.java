package com.bunevich.study_project.lessons.final_project.Product.item;

import com.bunevich.study_project.lessons.final_project.Product.Dairy;

public class Cheese extends Dairy {

    public Cheese(String type, String brand, int cost,  int storage_temperature) {
        super(type, brand, cost, storage_temperature);
    }
}
