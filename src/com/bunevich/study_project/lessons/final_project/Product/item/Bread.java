package com.bunevich.study_project.lessons.final_project.Product.item;


import com.bunevich.study_project.lessons.final_project.Product.Bakery;

public class Bread extends Bakery {

    public Bread(String type, String brand, int cost,  String shelf_life, boolean friability) {
        super(type, brand, cost, shelf_life, friability);
    }
}
