package com.bunevich.study_project.lessons.final_project.Product.item;


import com.bunevich.study_project.lessons.final_project.Product.Sweets;

public class Candy extends Sweets {

    public Candy(String type, String brand, int cost,  int amount_of_sugar) {
        super(type, brand, cost,  amount_of_sugar);
    }
}
