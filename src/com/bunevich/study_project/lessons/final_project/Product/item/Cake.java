package com.bunevich.study_project.lessons.final_project.Product.item;


import com.bunevich.study_project.lessons.final_project.Product.Sweets;

public class Cake extends Sweets {

    public Cake(String type, String brand, int cost,  int amount_of_sugar) {
        super(type, brand, cost,  amount_of_sugar);
    }
}
