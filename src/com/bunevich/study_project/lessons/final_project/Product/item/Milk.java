package com.bunevich.study_project.lessons.final_project.Product.item;


import com.bunevich.study_project.lessons.final_project.Product.Dairy;

public class Milk extends Dairy {

    public Milk(String type, String brand, int cost,  int storage_temperature) {
        super(type, brand, cost,  storage_temperature);
    }
}
