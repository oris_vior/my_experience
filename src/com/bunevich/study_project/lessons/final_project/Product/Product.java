package com.bunevich.study_project.lessons.final_project.Product;

public abstract class Product {
    private String type;
    private String brand;
    private int cost;


    public Product(String type, String brand, int cost ) {
        this.type = type;
        this.brand = brand;
        this.cost = cost;

    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public int getCost() {
        return cost;
    }

    public void setCost(int cost) {
        this.cost = cost;
    }


}
