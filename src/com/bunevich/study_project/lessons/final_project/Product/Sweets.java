package com.bunevich.study_project.lessons.final_project.Product;

public class Sweets extends Product {
    private final int amountOfSugar;

    public Sweets(String type, String brand, int cost, int amountOfSugar) {
        super(type, brand, cost);
        this.amountOfSugar = amountOfSugar;
    }
}
