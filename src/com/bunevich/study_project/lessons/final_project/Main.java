package com.bunevich.study_project.lessons.final_project;


import com.bunevich.study_project.lessons.final_project.list.Menu;
import com.bunevich.study_project.lessons.final_project.list.ShoppingList;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {
    private final ShoppingList shoppingList = new ShoppingList();

    public static void main(String[] args) {
        Main main = new Main();
        main.startApplication();

    }

    public static String inputUser() {
        String input = "";
        try {
            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            input = reader.readLine();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return input;
    }

    private void startApplication() {
        Menu menu = new Menu();
        System.out.println("Welcome to your ShoppingList");
        System.out.println("");
        String userInput = "";


        while (!userInput.equals("-exit")) {
            menu.startMenu();
            userInput = inputUser();
            checkCommand(userInput);
        }

    }

    private void checkCommand(String input) {
        switch (input) {
            case ("-add") -> System.out.println(shoppingList.addProductToList(inputUser()));
            case ("-view1") -> shoppingList.viewProducts();
            case ("-view2") -> shoppingList.viewShoppingList();
            case ("-remove1") -> shoppingList.removeElementForIndex(inputUser());
            case ("-remove2") -> shoppingList.removeElement(inputUser());
            case ("-exit") -> System.out.println("Bye");
            default -> System.out.println("Command not found");
        }

    }


}
