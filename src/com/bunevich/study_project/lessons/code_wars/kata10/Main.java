package com.bunevich.study_project.lessons.code_wars.kata10;


import java.lang.reflect.Array;
import java.util.Arrays;

public class Main {
    public static void main(String[] args) {

        System.out.println(Arrays.toString(DeadFish.parse("iiisdoso")));
    }





    public static class DeadFish {
        public static int[] parse(String data) {
            data = data.trim().toLowerCase();
            String[] arr = data.split("");
            int check = 0;
            int number = 0;
            for(int i = 0; i< data.length(); i++){

                if (arr[i].equals("o")){
                    check++;
                }
            }
            int[] array = new int[check];
            check = 0;
            for(int i = 0 ; i< data.length(); i++){
                switch (arr[i]){
                    case "i": number ++; break;
                    case "d": number--;  break;
                    case "s": number= number*number; break;
                    case "o": array[check] = number; check ++;   break;
                    default: ;
                }
            }
            return array;
        }
    }



}
