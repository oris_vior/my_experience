package com.bunevich.study_project.lessons.code_wars.kata3;
//https://www.codewars.com/kata/546e2562b03326a88e000020/train/java


public class SquareEveryDigit {
    public static void main(String[] args) {
        System.out.println(new SquareDigit().squareDigits(9119));
    }



    public static class SquareDigit {

        public int squareDigits(int n) {
            StringBuilder resultInString = new StringBuilder();
            String number = Integer.toString(n);
            char[] array = number.toCharArray();
            for(int i = 0 ; i< array.length; i++){
                int check = Integer.parseInt(String.valueOf(array[i]));
                check = check * check;
                resultInString.append(check);
            }
            return Integer.parseInt(String.valueOf(resultInString));
        }

    }
}
