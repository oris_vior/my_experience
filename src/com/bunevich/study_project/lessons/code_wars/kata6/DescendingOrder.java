package com.bunevich.study_project.lessons.code_wars.kata6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

public class DescendingOrder {
    public static void main(String[] args) {
        int num = 123456789;
        sortDesc(num);

    }
        public static int sortDesc( int num) {

           int [] number = new int[String.valueOf(num).length()];

            for( int i = 0; i < number.length; i++){
                number[i] = num % 10;
                num /= 10;

            }
            Arrays.sort(number);
            for (int i = 0; i < number.length / 2; i++) {
                int tmp = number[i];
                number[i] = number[number.length - i - 1];
                number[number.length - i - 1] = tmp;
            }
            int result = 0;

            for (int i =number.length -1 , n = 0; i >= 0; --i, n++) {
                int pos = (int)Math.pow(10, i);
                result += number[n] * pos;
            }

            System.out.println(result);
        return num;
    }
}
