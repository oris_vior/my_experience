package com.bunevich.study_project.lessons.code_wars.kata6;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class ListFiltering {
    public static void main(String[] args) {
        List<Object> list = new ArrayList<>();
        list.add(1);
        list.add(2);
        list.add("3");
        list.add(4);
        System.out.println(list);
        filterList(list);
        System.out.println(list);

        System.out.println(filterList(Arrays.asList(new Object[]{1,2,"a","b"})));
    }

    public static List filterList(final List list) {
        List<Object> list1 = new ArrayList<>();
        for (Object i : list) {
            if ((i instanceof Integer)) {
                list1.add(i);
            }
        }
        return list1;
    }
}

