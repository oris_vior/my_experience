package com.bunevich.study_project.lessons.code_wars.kata8;

public class Main {
    public static void main(String[] args) {
        countBits(12);
    }
    public static int countBits(int n ){
        int number = 0 ;
        String check = Integer.toBinaryString(n);
        for (int i = 0 ; i< check.length(); i++){
            if('1' == (check.charAt(i))){
                number ++;
            }
        }
        return number;
    }
}
