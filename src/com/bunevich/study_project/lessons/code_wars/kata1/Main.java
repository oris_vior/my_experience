package com.bunevich.study_project.lessons.code_wars.kata1;
//https://www.codewars.com/kata/5572392fee5b0180480001ae/train/java
public class Main {
    public static void main(String[] args) {
        Bud bun = new Bud();
        System.out.print( bun.computerToPhone("123789"));
    }

    public static class Bud {
        public  String computerToPhone(String number){
            number = number.replaceAll("1","!");
            number =number.replaceAll("2","@");
            number =number.replaceAll("3","#");
            number =number.replaceAll("7","1");
            number =number.replaceAll("8","2");
            number =number.replaceAll("9","3");
            number =number.replaceAll("!","7");
            number =number.replaceAll("@","8");
            number =number.replaceAll("#","9");
            return number;
        }
    }
}
