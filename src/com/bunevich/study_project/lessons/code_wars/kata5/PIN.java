package com.bunevich.study_project.lessons.code_wars.kata5;
//https://www.codewars.com/kata/55f8a9c06c018a0d6e000132/train/java
public class PIN {
    public static void main(String[] args) {
        System.out.println(validatePin("1234"));

    }

    public static boolean validatePin(String pin) {

        if(pin.matches("[0-9]") && (pin.length()== 4 || pin.length() ==6) ){
        return true;
        }
        return false;
    }
}
