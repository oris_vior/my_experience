package com.bunevich.study_project.lessons.code_wars.kata2;
//https://www.codewars.com/kata/514b92a657cdc65150000006/train/java
public class Main {
    public static void main(String[] args) {

        System.out.println(new Solution().solution(10));
    }





    public static class Solution {

        public int solution(int number) {
            int summ = 0;
            for(int i=0; i<number; i++){
                if(number>0 && (i % 3 ==0 || i%5 ==0)) {
                    summ = summ + i;
                }
            }
            return summ;
        }
    }
}
