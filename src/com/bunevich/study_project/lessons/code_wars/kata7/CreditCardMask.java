package com.bunevich.study_project.lessons.code_wars.kata7;

public class CreditCardMask {
    public static void main(String[] args) {
        System.out.println(maskify("123456789"));
    }

    public static String maskify(String str) {
        String temp = str;
        if (str.length() > 4) {
            str = str.trim().substring(0, str.length() - 4).replaceAll(".", "#");
            temp = str + temp.substring(temp.length() - 5, temp.length() - 1);
        } else {
            temp = str.trim().replaceAll(".", "#");
        }

        return temp;
    }
}
