package com.bunevich.study_project.lessons.code_wars.kata11;

import java.math.BigInteger;

public class Main {
    public static void main(String[] args) {
        System.out.println(SumFct.perimeter(new BigInteger("30")));
    }

    public class SumFct {
        public static BigInteger perimeter(BigInteger n) {
            BigInteger k = new BigInteger("0");
            BigInteger i = new BigInteger("0");
            BigInteger j = new BigInteger("1");
            BigInteger sum = new BigInteger("0");
            for(int q = 0; q != n.intValue(); q++){
                k = i.add(j);
                i = j;
                j = k;
                sum = sum.add(k.multiply(new BigInteger("4")));
            }

            return  sum.add(new BigInteger("4"));
        }
    }
}
