package com.bunevich.study_project.lessons.code_wars.kata12;

public class Main {
    public static void main(String[] args) {
        HumanReadableTime.makeReadable(86399);
    }


    public static class HumanReadableTime {
        public static boolean makeReadable(int seconds) {
            int hour = 0;
            int minute = 0;
            int second = 0;
            String hour1;
            hour = seconds/3600;
            if(hour<10){
                 hour1 = "0"+hour;
            }
            else {
                 hour1 = String.valueOf(hour);
            }
            minute = ( seconds - (hour * 3600) ) / 60;
            String min1;
            if(minute<10){
                 min1 = "0"+minute;
            }
            else {
                 min1 = String.valueOf(minute);
            }
            second = seconds - (hour*3600) - (minute*60);
            String sec1;
            if(second<10){
                 sec1 = "0"+second;
            }
            else {
                 sec1 = String.valueOf(second);
            }

            String str = hour1 +":" + min1+":"+ sec1;
            System.out.println(str);
            return false;
        }
    }
}
