package com.bunevich.study_project.lessons.reflextion;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

public class CastToOtherType {
    public static void main(String[] args) {

        Collection<Integer> digital = List.of(1,5,-33,0);
        Collection<Integer> sorted = digital.stream().sorted(Comparator.reverseOrder()).collect(Collectors.toList());
        System.out.println(sorted);

    }
}
