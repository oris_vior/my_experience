package com.bunevich.study_project.lessons.lesson14;


import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import static com.bunevich.study_project.lessons.lesson14.ConstantUtil.LINE_SEPARATOR;
import static com.bunevich.study_project.lessons.lesson14.ConstantUtil.PASSWORD;
import static com.bunevich.study_project.lessons.lesson14.ConstantUtil.SELECT_ALL_PRODUCTS;
import static com.bunevich.study_project.lessons.lesson14.ConstantUtil.URL;
import static com.bunevich.study_project.lessons.lesson14.ConstantUtil.USERNAME;

public class Main {


    public static void main(String[] args) throws ClassNotFoundException {

        try (Connection connection = DriverManager.getConnection(URL, USERNAME, PASSWORD)) {
            Statement statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_ALL_PRODUCTS);
            while (resultSet.next()) {
                System.out.println("ID " + resultSet.getInt("id"));
                System.out.println("Display name  " + resultSet.getString("display_name"));
                System.out.println("Number in stock " + resultSet.getInt("number_in_stock"));
                System.out.println(LINE_SEPARATOR);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

}
