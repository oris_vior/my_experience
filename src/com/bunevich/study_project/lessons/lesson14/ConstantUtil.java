package com.bunevich.study_project.lessons.lesson14;

public class ConstantUtil {
    public static final String URL = "jdbc:mysql://localhost:3306/product";
    public static final String USERNAME = "root";
    public static final String PASSWORD = "root";
    public static final String SELECT_ALL_PRODUCTS = "SELECT * FROM product";


    public static final String LINE_SEPARATOR = "====================================";
}
