package com.bunevich.study_project.lessons.lesson13.Multithreading;

public class Counter {
    private volatile int counter;


    public void increment() {
        counter++;
    }


    public void decrement() {
        counter--;
    }


    public synchronized int getCounter() {
        return counter;
    }

}
