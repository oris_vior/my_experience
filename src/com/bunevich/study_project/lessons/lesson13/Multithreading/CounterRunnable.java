package com.bunevich.study_project.lessons.lesson13.Multithreading;

import java.util.function.Consumer;

public class CounterRunnable implements Runnable {
    private final Counter counter ;
    private final Consumer<Counter> counterConsumer;

    public CounterRunnable(Counter counter, Consumer<Counter> counterConsumer) {
        this.counter = counter;
        this.counterConsumer = counterConsumer;
    }

    @Override
    public void run() {
        synchronized (counter) {
            counterConsumer.accept(counter);

        }
    }
}
