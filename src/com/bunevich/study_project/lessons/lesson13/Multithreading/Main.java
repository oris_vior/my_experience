package com.bunevich.study_project.lessons.lesson13.Multithreading;


public class Main {
    public static void main(String[] args) throws InterruptedException {
        Counter counter = new Counter();

        Thread incrementThread = new Thread(new CounterRunnable(counter, (counter1) -> {
            while (counter.getCounter() <= 10) {
                counter.increment();
                System.out.println("increment " + counter.getCounter());
                try {
                    Thread.sleep(10L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Counter is in maximum");
        }));


        Thread decrementThread = new Thread(new CounterRunnable(counter, (counter1)->{
            while (counter.getCounter()>=0){
                counter.decrement();
                System.out.println("decremented " + counter.getCounter());
                try {
                    Thread.sleep(10L);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Counter is in minimal");
        }));
        incrementThread.start();
        decrementThread.start();

    }

}
