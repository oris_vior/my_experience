package com.bunevich.study_project.lessons.lesson2;


import com.bunevich.study_project.lessons.calculator.SwichAndIf;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Lesson2 {

    public static void main(String[] args) throws IOException {

        BufferedReader simplein = new BufferedReader(new InputStreamReader(System.in));

        SwichAndIf ExampleSwichOne = new SwichAndIf();
        System.out.print("Enter your attempts: ");
        int attempts = Integer.parseInt(simplein.readLine());


        while (attempts != 0 ) {
            System.out.print("Enter day of week: ");
            String extertext = simplein.readLine();
            System.out.println(ExampleSwichOne.ExampleSwichCase(extertext));
            System.out.println(ExampleSwichOne.ExampleIfElse(extertext));
            attempts--;
            if(attempts == 1)
            {
                System.out.println("Last try");
            }
        }
        int number = 10;
        do{
            number += 5;
            if(number > 31){
                System.out.println("Fail...");
            }
        }
        while (number < 31);



    }


}


