package com.bunevich.study_project.lessons.lesson10;

import java.io.File;

public class Main {
    public static void main(String[] args) {
        Main demo = new Main();
        String path = "./src/com/itea/bunevich";
        File file = new File(path);
        demo.FileInDir(file);
//        if(file.isDirectory()){
//            for (File item: file.listFiles()){
//                if(item.isDirectory()){
//                    System.out.println("Direcroty: " + item);
//
//                }
//                else {
//                    System.out.println("File: " + item);
//                }
//            }
//        }


    }

    private void FileInDir(File file){
        if(file.isDirectory()){
            for (File item: file.listFiles()) {
                File temp_file = new File(item.getPath());
                if(item.isFile()){
                    System.out.println("  File: " + item.getName());
                }
                else {
                    System.out.println("Dir: " + item.getName());
                    FileInDir(temp_file);
                }

        }
    }

    }
}
