package com.bunevich.study_project.lessons.command;

public class Blue implements Command {

    private Light theLight;

    public void Blue(Light theLight){
        this.theLight = theLight;
    }

    @Override
    public void execute() {
        theLight.Blue();
    }
}
