package com.bunevich.study_project.lessons.command;

public class Green implements Command {
    private Light theLight;

    public void setTheGreen(Light theLight){
        this.theLight = theLight;
    }

    @Override
    public void execute(){
        theLight.Green();
    }
}
