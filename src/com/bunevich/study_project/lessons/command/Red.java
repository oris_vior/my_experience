package com.bunevich.study_project.lessons.command;

public class Red implements Command {

    private Light theLight;

    public void setTheRed(Light light) {
        this.theLight = light;
    }

    @Override
    public void execute(){
        theLight.Red();
    }


}
