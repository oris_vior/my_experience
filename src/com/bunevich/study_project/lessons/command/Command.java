package com.bunevich.study_project.lessons.command;

public interface Command {
   void execute();
}
