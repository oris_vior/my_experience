package com.bunevich.study_project.lessons.clone;

public class Demo {

    public static void main(String[] args) throws CloneNotSupportedException {
        Department dept = new Department(1, "Human Resource");
        Employee original = new Employee(1, "Admin", dept);

        //Lets create a clone of original object
        Employee cloned = (Employee) original.clone();
        original.getDepartment().setName("Igor");
        original.setEmpoyeeId(12);

        //Let verify using employee id, if cloning actually workded
        System.out.println(original.getDepartment().getName());
        System.out.println(cloned.getDepartment().getName());
        System.out.println(original.getEmpoyeeId());
        System.out.println(cloned.getEmpoyeeId());
    }
}
