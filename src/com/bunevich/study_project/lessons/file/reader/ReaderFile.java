package com.bunevich.study_project.lessons.file.reader;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class ReaderFile implements ReadFile {
    List<String> listReader= new ArrayList<>();

    @Override
    public List<String> file(){
        File file = new File("src/com/bunevich/study_project/lessons/file/Files/excel.csv");
        try (BufferedReader reader = new BufferedReader(new FileReader(file))) {
            String line;
            while ((line = reader.readLine()) != null){
                listReader.add(line);
            }
        } catch (IOException e) {
            System.out.println("Error: " + e.getMessage());
        }
        return listReader;
    }
}
