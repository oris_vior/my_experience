package com.bunevich.study_project.lessons.file.reader;

import java.util.List;

public interface ReadFile {
    List<String> file();
}
