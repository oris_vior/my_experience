package com.bunevich.study_project.lessons.file;

import com.bunevich.study_project.lessons.file.reader.ReadFile;
import com.bunevich.study_project.lessons.file.reader.ReaderFile;

import java.io.FileReader;
import java.io.FileWriter;
import java.math.BigDecimal;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {

        ReaderFile readerFile = new ReaderFile();
        SortData sortData= new SortData(readerFile);

       sortData.showPrice();
        sortData.showName();
    }
}
