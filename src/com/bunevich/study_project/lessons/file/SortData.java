package com.bunevich.study_project.lessons.file;

import com.bunevich.study_project.lessons.file.reader.ReadFile;

public class SortData {
    private final ReadFile readFile;

    public SortData(ReadFile readFile) {
        this.readFile = readFile;
    }

    public void showName(){
        readFile.file().forEach(s -> System.out.println(s.split(";")[1]));
    }
    public void showPrice(){
        readFile.file().forEach(s -> System.out.println(s.split(";")[1]));
    }

}
