package com.bunevich.study_project.lessons;

import java.util.Random;



public class Test {

    final static int RANDOM_NUMBER = 2;
    final static int SIZE_ARRAY = 10;


    public static void main(String[] args) {
        Random rand = new Random();
        int[] array = new int[SIZE_ARRAY];
        for (int i = 0; i < array.length; i++) {
            array[i] = rand.nextInt(RANDOM_NUMBER);
            System.out.print(array[i]+ " ");
        }
        System.out.println("\n"+ "Max " + maxValue(array));
        System.out.println("Min " + minValue(array));
        System.out.println("Frequency " + frequent(array));
        System.out.println("Graph ");
        graph(array);
    }

    public static int maxValue(int[] array) {
        int temp = 0;
        for (int i = 0; i < array.length; i++) {
            if (temp < array[i]) {
                temp = array[i];
            }
        }
        return temp;
    }

    public static int minValue(int[] array) {
        int temp = 51;
        for (int i = 0; i < array.length; i++) {
            if (temp > array[i]) {
                temp = array[i];
            }
        }
        return temp;
    }

    public static int frequent(int[] array) {
        int maxNumber = 0;
        int freq = 0;
        for (int i = 0; i < array.length; i++) {
            for (int j = 0; j < array.length; j++) {
                if (array[i] == array[j]) {
                    freq ++;
                }
            }
            if (freq > maxNumber) {
                maxNumber = array[i];

            }
            freq = 0;
        }
        return maxNumber;
    }

    public static void graph(int[] array) {
        int counter = 0;
        int temp = 0;
        String star = "";
        for (int i = 0; i < array.length; i++) {
            temp = array[i];
            for (int j = array.length - 1; j >= i; j--) {
                if (temp == -1) {
                    continue;
                }
                if (temp == array[j]) {
                    counter++;
                    star += "*";
                    array[j] = -1;
                }
            }
            if (temp == -1) {
                continue;
            }
            System.out.println("array ["+ temp + "] : " + counter + " " + star);
            counter = 0;
            star = "";
        }
    }


}

