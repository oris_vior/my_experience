package com.bunevich.study_project.lessons.lesson11;

import java.io.Serializable;

public class Person implements Serializable {

    private String name;
    private int age;
    private int password;

    public Person(String name, int age, int password){
        this.name = name;
        this.age = age;
        this.password = password;
    }

    public String getName() {
        return name;
    }


    public int getAge() {
        return age;
    }

       public int getPassword() {
        return password;
    }



    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                ", password=" + password +
                '}';
    }
}
