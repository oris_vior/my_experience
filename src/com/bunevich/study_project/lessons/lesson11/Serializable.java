package com.bunevich.study_project.lessons.lesson11;

import java.io.*;
import java.util.List;

public class Serializable {
    public static void main(String[] args) throws IOException {

        Person first = new Person("Igor", 20, 62780);
        Person second = new Person("Yura", 21, 62781);
        Person third = new Person("Masha", 22, 62782);

        List<Person> people = List.of(first,second,third);


            ObjectOutputStream obj = new ObjectOutputStream(
                    new FileOutputStream("person.out"));
            obj.writeObject(people);
            obj.close();


        ObjectInputStream objIn = new ObjectInputStream(new FileInputStream("person.out"));
        try {
            List Restored = (List) objIn.readObject();
            System.out.println(Restored);
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
}
